from django.shortcuts import render

from portafolio.myprofile.models import Profile


def home(request):

	profile = Profile.objects.first()
	context = {
		'profile': profile,
		'skills': profile.skills.by(3)
	}

	return render(request, 'home/index.html', context)
