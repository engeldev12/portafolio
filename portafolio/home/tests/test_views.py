from django.test import TestCase
from django.urls import reverse

from portafolio.myprofile.factories import ProfileFactory, SkillFactory


class HomeViewTest(TestCase):

	def setUp(self):
		self.url = reverse('home:index')
		self.profile = ProfileFactory.create()

	def test_tengo_un_perfil_con_habilidades(self):

		SkillFactory.create(
			profile=self.profile
		)

		response = self.client.get(self.url)

		self.assertEqual("Engel", response.context['profile'].name)
		self.assertEqual(1, len(response.context['skills']))

	def test_solo_apareceran_tres_habilidades_en_el_home_asi_agregue_mas(self):

		SkillFactory.create(
			name='CI/CD',
			profile=self.profile
		)

		SkillFactory.create(
			name='BDs',
			profile=self.profile
		)

		SkillFactory.create(
			name='TDD',
			profile=self.profile
		)

		SkillFactory.create(
			name='Clean Code',
			profile=self.profile
		)

		response = self.client.get(self.url)

		self.assertEqual(3, len(response.context['skills']))
