from django.views import generic

from .models import Skill


class SkillsByProfileListView(generic.ListView):
	template_name = 'myprofile/skills.html'
	context_object_name = 'skills'

	def get_queryset(self):
		return Skill.objects.filter(profile__pk=self.kwargs['pk'])
