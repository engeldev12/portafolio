from django.db import models

from .querysets import SkillQuerySet


class SkillManager(models.Manager):

	def get_queryset(self):
		return SkillQuerySet(self.model, using=self._db)

	def by(self, quantity):
		return self.get_queryset().by(quantity)
