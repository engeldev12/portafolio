from django.contrib import admin

from .forms import ProfileCreateForm
from .models import Profile, Skill


class SkillStacked(admin.StackedInline):
	model = Skill
	extra = 1


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
	list_display = ['name', 'last_name', 'email']
	form = ProfileCreateForm
	inlines = [SkillStacked]
