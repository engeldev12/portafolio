from django.db import models

from .managers import SkillManager


class Profile(models.Model):
	name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=30)
	email = models.EmailField(unique=True)
	biography = models.TextField(max_length=255)
	address = models.CharField(max_length=80)
	photo = models.ImageField(upload_to="avatars", blank=True)
	phone_number = models.CharField(max_length=11, blank=True)

	def __str__(self):
		return "{name} {last_name}".format(
			name=self.name,
			last_name=self.last_name
		)


class Skill(models.Model):
	name = models.CharField(max_length=40)
	description = models.TextField()
	photo = models.ImageField(upload_to='skills', blank=True)
	profile = models.ForeignKey(
		Profile,
		related_name='skills',
		on_delete=models.CASCADE
	)
	objects = SkillManager()

	def __str__(self):
		return self.name
