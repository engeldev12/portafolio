from django.urls import path

from . import views

app_name = 'profile'

urlpatterns = [
	path(
		'<int:pk>/skills',
		views.SkillsByProfileListView.as_view(),
		name='skills'
	)
]
