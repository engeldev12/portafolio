import re

from django import forms

from .models import Profile
from .utils import is_empty


class ProfileCreateForm(forms.ModelForm):
	class Meta:
		model = Profile
		fields = [
			'name',
			'last_name',
			'email',
			'address',
			'biography',
			'photo',
			'phone_number'
		]

	def clean_phone_number(self):
		phone_number = self.cleaned_data['phone_number']

		phone_number_pattern = re.compile(r'\d{4}\d{3}\d{2}\d{2}')

		if not is_empty(phone_number):
			if phone_number_pattern.match(phone_number) is None:
				msj = "Esto no es un número de teléfono vádido {0}.".format(phone_number)
				raise forms.ValidationError(msj)

		return phone_number
