from factory.django import DjangoModelFactory
from factory import SubFactory

from .models import Profile, Skill


class ProfileFactory(DjangoModelFactory):
	class Meta:
		model = Profile

	name = 'Engel'
	last_name = 'Pinto'
	email = 'engeljavierpinto@gmail.com'
	address = 'New York'
	biography = 'My biography es muy buena'
	phone_number = '04161231212'


class SkillFactory(DjangoModelFactory):
	class Meta:
		model = Skill

	name = 'CI/CD'
	description = 'Integración Continua'
	profile = SubFactory(ProfileFactory)
