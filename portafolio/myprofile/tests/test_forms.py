from django.test import TestCase

from ..forms import ProfileCreateForm

from ..factories import ProfileFactory


class ProfileCreateFormTest(TestCase):

	def setUp(self):
		self.data = {
			'name': "Engel",
			'last_name': "Pinto Ruiz",
			'email': "engeljavierpinto@gmail.com",
			'address': "New York",
			'biography': "Soy un desarrollador de software",
			'phone_number': "04163871212"
		}

	def test_la_foto_del_perfil_es_opcional(self):

		form = ProfileCreateForm(self.data)

		self.assertTrue(form.is_valid())

	def test_no_se_puede_repetir_el_email(self):

		ProfileFactory.create(
			email="engeljavierpinto@gmail.com"
		)
		self.data['email'] = "engeljavierpinto@gmail.com"
		form = ProfileCreateForm(self.data)

		self.assertFalse(form.is_valid())

	def test_debe_tener_una_direccion_el_perfil(self):

		self.data['address'] = ''
		form = ProfileCreateForm(self.data)

		self.assertFalse(form.is_valid())

	def test_numero_de_telefono_invalido(self):

		self.data['phone_number'] = '234sq345'

		form = ProfileCreateForm(self.data)

		self.assertFalse(form.is_valid())

	def test_es_opcional_el_numero_de_telefono(self):

		self.data['phone_number'] = ''
		form = ProfileCreateForm(self.data)

		self.assertTrue(form.is_valid())
