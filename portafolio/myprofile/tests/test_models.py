from django.test import TestCase

from ..factories import SkillFactory
from ..models import Skill


class SkillTest(TestCase):

	def test_se_puede_filtrar_la_cantidad_de_skill(self):

		SkillFactory.create()

		self.assertEqual(1, Skill.objects.by(2).count())

	def test_no_hay_skill(self):

		self.assertEqual(0, Skill.objects.by(3).count())
