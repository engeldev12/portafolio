from django.test import TestCase
from django.urls import reverse

from ..factories import SkillFactory


class SkillsByProfileListViewTest(TestCase):

	def test_puedo_ver_el_listado_de_mis_habilidades(self):

		skill = SkillFactory.create(name="TDD")
		profile = skill.profile
		url = reverse('profile:skills', kwargs={'pk': profile.pk})

		response = self.client.get(url)

		self.assertIn(b"TDD", response.content)
