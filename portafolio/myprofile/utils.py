def is_empty(text):
	return not text.isspace() and len(text) == 0
