from django.db import models


class SkillQuerySet(models.QuerySet):

	def by(self, quantity):
		return self.all()[:quantity]
