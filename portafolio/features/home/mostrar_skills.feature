Característica: Mostrar información de Skill
	Como usuario
	Quiero ver mi información de skill en la página principal
	Para que los clientes conozcan mis habilidades

	Escenario: Ver mis habilidades
		Dado que tengo como habilidad Crear bases de datos
		Cuando la agregue a mis habilidades
		Entonces debe aparecer Crear bases de datos en el home

	Escenario: No tengo habilidades agregadas
		Dado que no he agregado ningún skill a mi perfil
		Cuando vaya a la página principal en la se sección skill
		Entonces debe decir Aún no tiene habilidades agregadas.

	Escenario: Ir al listado de mis habilidades
		Dado que cree mi habilidad de TDD
		Y estoy en el home
		Cuando vaya al listado de mis habilidades
		Entonces debe aparecer TDD en el listado.