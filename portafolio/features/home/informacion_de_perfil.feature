Característica: Ver información del perfil
	Como usuario 
	quiero poder ver la información de mi perfil en la página principal
	Para qué los clientes sepan quién soy.

	Escenario: Ver información al abrir la página
		Dado que creé mi perfil
		Cuando vaya a la página principal
		Entonces debe aparecer mi información