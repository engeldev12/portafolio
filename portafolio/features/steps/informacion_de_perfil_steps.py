from django.urls import reverse

from behave import given, when, then

from portafolio.myprofile.factories import ProfileFactory


@given('que creé mi perfil')
def step_impl(context):

	context.profile = ProfileFactory.create()


@when('vaya a la página principal')
def step_impl(context):

	context.browser.get(context.get_url(reverse('home:index')))


@then('debe aparecer mi información')
def step_impl(context):

	photo = context.browser.find_element_by_id('photo')
	biography = context.browser.find_element_by_id("biography").text
	address = context.browser.find_element_by_id("address").text
	phone_number = context.browser.find_element_by_id('phone-number').text

	context.test.assertEqual("default", photo.get_attribute('data-name'))
	context.test.assertEqual(context.profile.biography, biography)
	context.test.assertEqual(context.profile.address, address)
	context.test.assertEqual(context.profile.phone_number, phone_number)
