from django.urls import reverse

from behave import given, when, then

from portafolio.myprofile.factories import ProfileFactory, SkillFactory


@given('que tengo como habilidad {name}')
def step_impl(context, name):

	context.skill_name = name


@when('la agregue a mis habilidades')
def step_impl(context):

	SkillFactory.create(
		name=context.skill_name
	)


@then('debe aparecer {skill} en el home')
def step_impl(context, skill):

	context.browser.get(context.get_url(reverse('home:index')))

	text = context.browser.find_element_by_tag_name('h4[data-skill]').text
	photo = context.browser.find_element_by_tag_name('img[data-skill]')

	context.test.assertEqual(text, skill)
	context.test.assertEqual('default', photo.get_attribute('data-skill'))


# No tengo Skill agregados

@given('que no he agregado ningún skill a mi perfil')
def step_impl(context):

	profile = ProfileFactory.create()

	context.test.assertEqual(0, profile.skills.all().count())


@when('vaya a la página principal en la se sección skill')
def step_impl(context):

	context.browser.get(context.get_url(reverse('home:index')))


@then('debe decir {msj}')
def step_impl(context, msj):

	text = context.browser.find_element_by_tag_name('h5[data-skill-info]').text

	context.test.assertEqual(msj, text)


# Ir al listado de mis habilidades

@given('que cree mi habilidad de {skill}')
def step_impl(context, skill):

	context.skill = SkillFactory.create(name=skill)


@given('estoy en el home')
def step_impl(context):

	context.browser.get(context.get_url(reverse('home:index')))


@when('vaya al listado de mis habilidades')
def step_impl(context):

	context.browser.find_element_by_id('link-skills').click


@then('debe aparecer {skill} en el listado.')
def step_impl(context, skill):

	text = context.browser.find_element_by_tag_name('h4[data-skill]').text

	context.test.assertEqual(skill, text)
