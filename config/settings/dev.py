from .base import *


DEBUG = True

EMAIL_BACKENDS = "django.core.mail.backends.console.EmailBackend"

INSTALLED_APPS += [
	'behave_django',
]

DATABASES = {
	'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': root('portafolio/db.sqlite3'),
    }
}


MEDIA_ROOT = root('portafolio/media')