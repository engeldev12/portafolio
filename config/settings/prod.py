import os

import dj_database_url

from .base import *


DEBUG = False

ALLOWED_HOSTS = ['.herokuapp.com']

# Configuración para el envio de email

EMAIL_BACKENDS = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_USER = os.environ.get("EMAIL_USER")
EMAIL_HOST_PASSWORD = os.environ.get("EMAIL_PASSWORD")

# Fin

AFTER_FROM_SECURITY_MIDDLEWARE = 1

MIDDLEWARE.insert(AFTER_FROM_SECURITY_MIDDLEWARE, 'whitenoise.middleware.WhiteNoiseMiddleware')

DATABASES = {
	'default': dj_database_url.config()
} 

STATIC_ROOT = root('static_root')

STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

MEDIA_ROOT = root('portafolio/media')